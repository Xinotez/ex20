﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public void GenerateCards()
        {
            int x = 750;
            int y = 150;
            int c = 1;
            for (int i = 0; i < 10; i++)
            {
                PictureBox p = new PictureBox();
                p.Image = global::E.Properties.Resources.card_back_red;
                p.Size = new System.Drawing.Size(80, 80);
                p.Location = new Point(x, y);
                p.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                p.TabIndex = 0;
                p.TabStop = false;
                p.Name = "pictureBox" + c.ToString();
                this.Controls.Add(p);
                x = x - 80;
                c++;
            }
            PictureBox w = new PictureBox();
            w.Image = global::E.Properties.Resources.card_back_blue;
            w.Size = new System.Drawing.Size(80, 80);
            w.Location = new Point(400, y-100);
            w.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            w.TabIndex = 0;
            w.TabStop = false;
            w.Name = "pictureBox" + c.ToString();
            this.Controls.Add(w);
            c++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
